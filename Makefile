build:
	podman build --tag lamp1804 -f ./1804/Dockerfile .

up:
	podman container run --name lamp1804-demo -p "8080:80" -v ${PWD}/app:/app:Z lamp1804

clean:
	podman rm lamp1804-demo
	podman image rm lamp1804
